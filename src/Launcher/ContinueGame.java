package Launcher;

import java.io.IOException;
import java.util.Scanner;

public class ContinueGame {
    public void play() throws IOException {
        Launcher controller = new Launcher();
        Scanner scanner = new Scanner(System.in);
        boolean loopControl = true;
        do {
            System.out.println("Continue? [Y/N]");
            switch (scanner.next()) {
                case "y", "Y":
                    controller.setLoopControl(true);
                    loopControl = false;
                    break;
                case "N", "n":
                    controller.setLoopControl(false);
                    loopControl = false;
                    break;
                default:
                    System.out.println("Wrong input \n");
            }
        } while (loopControl);
    }
}

package Launcher;

import GameEngine.Engine;
import gameDifficulty.Easy;
import gameDifficulty.Hard;

import java.io.IOException;
import java.util.Scanner;

public class Launcher {
    private boolean loopControl = true;
    public Launcher() throws IOException {
        control();
    }
    private void control() throws IOException {
        MainMenu mainMenu = new MainMenu();
        Scanner scanner = new Scanner(System.in);

        do {
            mainMenu.printOptions();
            switch (scanner.next()) {
                case "0":
                    loopControl = false;
                    break;
                case "1":
                    new Engine(new Easy());
                    break;
                case "2":
                    new Engine(new Hard());
                    break;
                default:
                    System.out.println("Wrong input");
            }
        } while (loopControl);
    }
    public void setLoopControl(boolean loopControl) {
        this.loopControl = loopControl;
    }
}

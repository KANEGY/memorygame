package GameEngine;

import gameDifficulty.Difficulty;

import java.util.Scanner;

public class ScannerValue {
    private int indexA1;
    private int indexB1;
    private int indexA2;
    private int indexB2;
    private Scanner scanner = new Scanner(System.in);

    public String getValue1(Difficulty difficulty) {

        String[][] matrix = difficulty.getMatrix();
        System.out.print("Enter row: ");
        char a = scanner.next().toLowerCase().charAt(0); //-97
        indexA1 = a - 97;

        System.out.print("Enter column: ");
        indexB1 = scanner.nextInt() -1;


        return matrix[indexA1][indexB1];
    }
    public String getValue2(Difficulty difficulty) {

        String[][] matrix = difficulty.getMatrix();
        System.out.print("Enter row: ");
        char a = scanner.next().toLowerCase().charAt(0); //-97
        indexA2 = a - 97;

        System.out.print("Enter column: ");
        indexB2 = scanner.nextInt() -1;


        return matrix[indexA1][indexB1];
    }

    public int getIndexA1() {
        return indexA1;
    }

    public int getIndexB1() {
        return indexB1;
    }

    public int getIndexA2() {
        return indexA2;
    }

    public int getIndexB2() {
        return indexB2;
    }
}

package GameEngine;

import Launcher.ContinueGame;
import gameDifficulty.Difficulty;

import java.io.IOException;

public class Engine {
    private static int chances;
    private static int words;

    public Engine(Difficulty difficulty) throws IOException {
        engine(difficulty);
    }

    public void engine(Difficulty difficulty) throws IOException {
        char ascii = 65;
        int score = 0;
        ContinueGame continueGame = new ContinueGame();
        ScannerValue scannerValue = new ScannerValue();
        AnswerMatrix answerMatrix = new AnswerMatrix(difficulty);
        String[][] answerArray = answerMatrix.getMatrix();
        String[][] mainArray = difficulty.getMatrix();

        chances = difficulty.getChances();
        words = difficulty.getWords() / 2;
        answerMatrix.printMatrix(difficulty);

        while (chances > 0 && words != difficulty.getWords()) {
            System.out.println("Chances left: " + chances);
            if (scannerValue.getValue1(difficulty).equals(scannerValue.getValue2(difficulty)) &&
                    (scannerValue.getIndexA1() != scannerValue.getIndexA2() && scannerValue.getIndexB1() != scannerValue.getIndexB2() &&
                            (!mainArray[scannerValue.getIndexA1()][scannerValue.getIndexA2()].
                                    equals(answerArray[scannerValue.getIndexA1()][scannerValue.getIndexB1()])) &&
                            !mainArray[scannerValue.getIndexA1()][scannerValue.getIndexA2()].
                                    equals(answerArray[scannerValue.getIndexA2()][scannerValue.getIndexB2()]))) {

                answerArray[scannerValue.getIndexA1()][scannerValue.getIndexB1()] = mainArray[scannerValue.getIndexA1()][scannerValue.getIndexB1()];
                answerArray[scannerValue.getIndexA2()][scannerValue.getIndexB2()] = mainArray[scannerValue.getIndexA2()][scannerValue.getIndexB2()];
                words++;
                score++;

            } else {
                chances--;
                answerArray[scannerValue.getIndexA1()][scannerValue.getIndexB1()] = mainArray[scannerValue.getIndexA1()][scannerValue.getIndexB1()];
                answerArray[scannerValue.getIndexA2()][scannerValue.getIndexB2()] = mainArray[scannerValue.getIndexA2()][scannerValue.getIndexB2()];

                for (int i = 0; i < difficulty.getLines(); i++) {
                    System.out.print(ascii);
                    ascii++;
                    for (int j = 0; j < 4; j++) {
                        System.out.print(answerArray[i][j]);
                    }
                    System.out.println();
                }
                answerArray[scannerValue.getIndexA1()][scannerValue.getIndexB1()] = " X";
                answerArray[scannerValue.getIndexA2()][scannerValue.getIndexB2()] = " X";
            }
            ascii = 65;
            System.out.println("Chances left: " + chances);
            System.out.println("Score: " + score);
            for (int i = 0; i < difficulty.getLines(); i++) {
                System.out.print(ascii);
                ascii++;
                for (int j = 0; j < 4; j++) {
                    System.out.print(answerArray[i][j]);
                }
                System.out.println();
            }
            ascii = 65;
        }
        if (words != difficulty.getWords()) {
            System.out.println("You WON!");
        } else {
            System.out.println("You LOST!");
        }
        continueGame.play();
    }
}

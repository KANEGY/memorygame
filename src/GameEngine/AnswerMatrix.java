package GameEngine;

import gameDifficulty.Difficulty;

public class AnswerMatrix {

    private static String[][] matrix;

    public AnswerMatrix(Difficulty difficulty) {
        generateAnswerMatrix(difficulty);
    }

    private void generateAnswerMatrix(Difficulty difficulty) {
        matrix = new String[difficulty.getLines()][4];

        for (int i = 0; i < difficulty.getLines(); i++) {
            for (int j = 0; j<4; j++){
                matrix[i][j] = " X";
            }
        }
    }

    public void printMatrix(Difficulty difficulty) {
        char ascii = 65;
        System.out.println("   1  2  3  4");
        for (int i = 0; i < difficulty.getLines(); i++) {
            System.out.print(ascii);
            ascii++;
            for (int j = 0; j < 4; j++) {
                System.out.print(" " + matrix[i][j]);
            }
            System.out.println();
        }
    }

    public String[][] getMatrix() {
        return matrix;
    }
}

package data;

import java.util.ArrayList;
import java.util.Collections;

public class RandomizeWordsList {
    public ArrayList<String> randomize(ArrayList<String> list) {
        Collections.shuffle(list);
        return list;
    }
}

package data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class LoadDataFromFile {
    public ArrayList<String> getDataToList() throws IOException {
        ArrayList<String> wordsList = new ArrayList<>();
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader("Words.txt"));
            String word;
            while ((word = bufferedReader.readLine()) != null) {
                wordsList.add(word);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            bufferedReader.close();
        }
        return wordsList;
    }
}


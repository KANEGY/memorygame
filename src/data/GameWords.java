package data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class GameWords {

    public ArrayList<String> getWords(ArrayList<String> x, int numberOfWords ) {
        ArrayList<String> list = new ArrayList<>();
        Random random = new Random();

        while (list.size() < numberOfWords) {
            String value = x.get(random.nextInt(x.size()));
            if (!list.contains(value)) {
                list.add(value);
            }
        }

        list.addAll(list);
        Collections.shuffle(list);

        return list;
    }
}

package gameDifficulty;

import java.io.IOException;

public class Easy extends Difficulty {
    public Easy() throws IOException {
        matrix = new String[2][4];
        chances = 10;
        words = 4;
        lines = 2;
        this.createMatrix();
    }
}

package gameDifficulty;

import java.io.IOException;

public class Hard extends Difficulty {
    public Hard() throws IOException {
        matrix = new String[4][4];
        chances = 15;
        words = 8;
        lines = 4;
        this.createMatrix();
    }
}

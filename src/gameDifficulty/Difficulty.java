package gameDifficulty;

import data.GameWords;
import data.LoadDataFromFile;
import data.RandomizeWordsList;

import java.io.IOException;
import java.util.ArrayList;

public abstract class Difficulty {
    protected int chances;
    protected int words;
    protected int lines;
    protected String[][] matrix;

    protected String[][] createMatrix() throws IOException {

        LoadDataFromFile loadDataFromFile = new LoadDataFromFile();
        RandomizeWordsList randomizeWordsList = new RandomizeWordsList();
        GameWords gameWords = new GameWords();
        ArrayList<String> list = gameWords.getWords(randomizeWordsList.randomize(loadDataFromFile.getDataToList()), words);

        int k = 0;
        for (int i = 0; i < lines; i++) {
            for (int j = 0; j < 4; j++) {
                matrix[i][j] = list.get(k);
                k++;
            }
        }
        return matrix;
    }

    public String[][] getMatrix() {
        return matrix;
    }

    public int getLines() {
        return lines;
    }

    public int getChances() {
        return chances;
    }

    public int getWords() {
        return words;
    }
}
